use actix_web::{http::Method, middleware, web, App, HttpRequest, HttpResponse, HttpServer};
use lazy_static::lazy_static;
use regex::Regex;

mod registry;

lazy_static! {
    static ref V2_MATCHER: regex::Regex = Regex::new(r"^/v2/$").unwrap();
    static ref BLOB_UPLOADS_INIT_MATCHER: regex::Regex =
        regex::Regex::new(r"/v2/(?P<path>.*)/blobs/uploads/$").unwrap();
    static ref BLOB_UPLOADS_MATCHER: regex::Regex =
        regex::Regex::new(r"/v2/(?P<path>.*)/blobs/uploads/(?P<digest>.*)$").unwrap();
    static ref COMMAND_MATCHER: regex::Regex =
        regex::Regex::new("/v2/(?P<path>.+?){0,1}(?P<command>(/blobs/|/blobs/uploads/)){0,1}((?P<identifier>[^/]+?)){0,1}$").unwrap();
}

//async fn handler(mut request: HttpRequest, mut body: web::Payload) -> HttpResponse {
async fn handler(mut request: HttpRequest, mut body: web::Payload) -> HttpResponse {
    log::info!("path: {}", request.path());
    log::info!("method: {}", request.method());
    log::info!("queryString: {}", request.query_string());
    let blobs_uploads_regex = Regex::new(r"/v2/(?P<path>.*)/blobs/uploads/").unwrap();
    let headers = request.headers();
    let request_path = request.path().to_owned();

    let mut path: Option<&str> = None;
    let mut command: Option<&str> = None;
    let mut identifier: Option<&str> = None;

    if let Some(captures) = COMMAND_MATCHER.captures(&request_path) {
        path = captures.name("path").map_or(None, |v| Some(v.as_str()));
        command = captures.name("command").map_or(None, |v| Some(v.as_str()));
        identifier = captures.name("identifier").map_or(None, |v| Some(v.as_str()));

        log::info!(
            "path: {:?} command: {:?} digest: {:?}",
            path,
            command,
            identifier
        );

        match (command, request.method(), identifier) {
            (None, &Method::GET, None) => {
                return registry::registry_base(&request);
            },
            (Some("/blobs/"), &Method::HEAD, _) => {
                return registry::blobs_head(&request, path, identifier).await;
            },
            (Some("/blobs/uploads/"), &Method::POST, None) => {
                return registry::blobs_uploads_post(&request, path);
            },
            (Some("/blobs/uploads/"), &Method::PATCH, Some(identifier)) => {
                return registry::blobs_uploads_patch(&mut request, &mut body, path, identifier).await;
            },
            (Some("/blobs/uploads/"), &Method::PUT, Some(identifier)) => {
                return registry::blobs_uploads_put(&mut request, &mut body, path, identifier).await;
            },
            _ => {
                log::error!("No command recognized");
                log::error!("{:?}", request.query_string());
                log::error!("{:?}", request.headers());
                return HttpResponse::NotImplemented().finish();
            }
        }
    } else {
        log::error!("Path not captured");
        return HttpResponse::NotImplemented().finish();
    }
}

#[actix_web::main]
async fn main() -> Result<(), std::io::Error> {
    fern::Dispatch::new()
        .level(log::LevelFilter::Info)
        .level_for("main", log::LevelFilter::Trace)
        .chain(std::io::stdout())
        .apply()
        .unwrap();

    log::info!("xxx");
    HttpServer::new(|| {
        App::new()
            .wrap(middleware::DefaultHeaders::new().header("X-Version", "0.2"))
            .wrap(middleware::Compress::default())
            .wrap(middleware::Logger::default())
            .app_data(web::PayloadConfig::new(100000000))
            .default_service(web::route().to(handler))
    })
    .bind("127.0.0.1:8080")?
    .workers(1)
    .run()
    .await
}
