use actix_web::{HttpRequest, HttpResponse, HttpMessage, FromRequest, web::Payload};
use tokio::io::AsyncWriteExt;
use futures_util::stream::StreamExt;  
use futures_util::stream::Stream;  
use tokio::io::{AsyncSeekExt, SeekFrom};

pub fn registry_base(_request: &HttpRequest) -> HttpResponse {
    HttpResponse::Ok()

            .insert_header(("Docker-Distribution-API-Version", "registry/2.0"))
        .finish()
}

pub async fn blobs_head(_request: &HttpRequest, path: Option<&str>, digest: Option<&str>) -> HttpResponse {
    match tokio::fs::metadata(format!("data/blobs/{}/blob", &path.unwrap())).await {
        Ok(_) => {
            HttpResponse::Ok().finish()
        },
        Err(e) => {
            log::error!("{:?}", e);
            HttpResponse::NotFound().finish()
        }
    }

}

// TODO check query string
pub fn blobs_uploads_post(_request: &HttpRequest, path: Option<&str>) -> HttpResponse {
        let upload_id_str = uuid::Uuid::new_v4().to_hyphenated_ref().to_string();

        match std::fs::create_dir(format!("{}/{}", "data/uploads/", upload_id_str,)) {
            Ok(_) => {
                return HttpResponse::Accepted()
                    .insert_header(("Docker-Upload-UUID", upload_id_str.as_str()))
                    .insert_header((
                        "Location",
                        format!("/v2/{}/blobs/uploads/{}", "xxx/hhh", &upload_id_str),
                    ))
                    .insert_header(("Range", "0-0"))
                    .finish();
            }
            Err(e) => {
                log::error!("{:?}", e);
                return HttpResponse::InternalServerError().finish();
            }
        }
}

pub async fn blobs_uploads_patch(request: &mut HttpRequest, body: &mut actix_web::web::Payload, path: Option<&str>, identifier: &str) -> HttpResponse {
    log::info!("{:?}", request.headers());

    let mut file = tokio::fs::OpenOptions::new().create(true).append(true).open(format!("data/uploads/{}/blob", identifier)).await.expect("xxx");
    log::info!("File created");

    let mut read: usize = 0;
    let mut wrotten: usize = 0;

    while let Some(chunk) = body.next().await {
        match chunk {
            Ok(bytes) => {
                read += bytes.len();
                wrotten += file.write(&bytes).await.unwrap();
            },
            Err(e) => {
                log::error!("{:?}", e);
            }
        }
    }
    log::info!("read {}, wrotten {}", read, wrotten);
    HttpResponse::NoContent()
        .insert_header(("Location", format!("/v2/{}/blobs/uploads/{}", &path.unwrap(), identifier)))
        .insert_header(("Range", format!("0-{}", file.metadata().await.unwrap().len())))
        .finish()
}

pub async fn blobs_uploads_put(request: &mut HttpRequest, body: &mut actix_web::web::Payload, path: Option<&str>, identifier: &str) -> HttpResponse {
    let qs: serde_json::Value = serde_urlencoded::from_str(request.query_string()).unwrap();

    log::info!("{:?}", qs);

    let filename = format!("data/uploads/{}/blob", identifier);
    let mut file = tokio::fs::File::open(&filename).await.expect("xxx");
    file.seek(SeekFrom::End(0)).await.expect("UUU");
    let file_len = file.metadata().await.expect("Failed to get file metadata").len();
    log::info!("{:?}", file_len);

    file.flush().await.unwrap();

    tokio::fs::create_dir_all(format!("data/blobs/{}", identifier)).await.unwrap();
    tokio::fs::copy(&filename, format!("data/blobs/{}/blob", identifier));

    HttpResponse::NoContent()
        .insert_header(("Location", format!("/v2/blobs/{}", identifier)))
        .insert_header(("Content-Range", format!("0-{}", file_len)))
        .insert_header(("Docker-Content-Digest", "blah"))
        //.insert_header(("Docker-Content-Digest", qs.get("digest").unwrap().as_str().unwrap()))
        .finish()
}

pub async fn xblobs_uploads_put(request: &mut HttpRequest, body: &mut actix_web::web::Payload, path: Option<&str>, identifier: &str) -> HttpResponse {
    let mut buf = actix_web::web::BytesMut::with_capacity(1024);

    while let Some(chunk) = body.next().await {
        match chunk {
            Ok(bytes) => {
                buf.extend_from_slice(&bytes);
            },
            Err(e) => {
                log::error!("{:?}", e);
            }
        }
    }
    
    let resp: serde_json::Value = serde_json::from_slice(&buf).unwrap();

    log::info!("{:?}", resp);
    HttpResponse::Ok().json(resp)
}
